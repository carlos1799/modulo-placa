import re

def detec(palabra):

    if re.match('MB[0-9]{4}$', palabra):
        tipo = "metrobus"

    elif re.match('BC[0-9]{4}$', palabra):
        tipo = "bus colegial"

    elif re.match('T[0-9]{5}$', palabra):
        tipo = "taxi"

    elif re.match('^M([A-Z][0-9]{4}$||^M[0-9]{5})$', palabra):
        tipo = "moto"

    elif re.match('8B[0-9]{4}', palabra):
        tipo = "coaster"

    elif re.match('CC[0-9]{4}$', palabra):
        tipo = "Cuerpo Consular"

    elif re.match('6H[0-9]{4}', palabra):
        tipo = "Cuerpo Honorario"

    elif re.match('MI[0-9]{4}', palabra):
        tipo = "mision internacional"

    elif re.match('CD[0-9]{4}', palabra):
        tipo = "placa de cuerpo diplomatico"

    elif re.match('E[0-9]{5}', palabra):
        tipo = "jueces o fiscales"

    elif re.match('ADM[0-9]{3}', palabra):
        tipo = "administracion"

    elif re.match('PR[0-9]{4}', palabra):
        tipo = "prensa"

    elif re.match('HP[0-9]{4}', palabra):
        tipo = "radio aficionados"

    elif re.match('[A-Z]{2}[0-9]{4}', palabra):
        tipo = "particular"

    elif re.match('([A-Z][0-9]{5}||[0-9])', palabra):
        tipo = "particular"

    return tipo