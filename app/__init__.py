from app import modulo
if __name__=='__main__':
    """
    se importa el modulo 
    """
    palabra=input("Introduzca la placa a evaluar: ")
    let = len(palabra)

    if let == 6:
        tipo = modulo.detec(palabra)
        print("El tipo de la placa evaluada es de", tipo)
    else:
        print("Tipo de placa invalida")